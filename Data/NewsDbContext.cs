﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data
{
    public class NewsDbContext : DbContext
    {
        public NewsDbContext(DbContextOptions<NewsDbContext> options) : base(options)
        {

        }

        #region News
        public DbSet<Models.News.NewsItem> NewsItem { get; set; }
        public DbSet<Models.News.NewsComment> NewsComment { get; set; }
        #endregion

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region News
            modelBuilder.ApplyConfiguration<Models.News.NewsItem>(new Models.Mapping.NewsMaping.NewsItemMap());
            modelBuilder.ApplyConfiguration<Models.News.NewsComment>(new Models.Mapping.NewsMapping.NewsCommentMap());
            #endregion
            base.OnModelCreating(modelBuilder);
        }

    }
}