﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Microsoft.EntityFrameworkCore
{
   
    public interface IFPRepositoryFactory
    {
        
        IFPDbRepository<TEntity> GetRepository<TEntity>() where TEntity : class;
    }
}
