﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Microsoft.EntityFrameworkCore
{
    
    public static class FPUnitOfWorkServiceCollectionExtensions
    {
        
        public static IServiceCollection AddUnitOfWork<TContext>(this IServiceCollection services) where TContext : DbContext
        {
            services.AddTransient<IFPRepositoryFactory, FPUnitOfWork<TContext>>();            
            services.AddTransient<IFPUnitOfWork, FPUnitOfWork<TContext>>();
            services.AddTransient<IFPUnitOfWork<TContext>, FPUnitOfWork<TContext>>();

            return services;
        }

        
        public static IServiceCollection AddUnitOfWork<TContext1, TContext2>(this IServiceCollection services)
            where TContext1 : DbContext
            where TContext2 : DbContext
        {
            services.AddTransient<IFPUnitOfWork<TContext1>, FPUnitOfWork<TContext1>>();
            services.AddTransient<IFPUnitOfWork<TContext2>, FPUnitOfWork<TContext2>>();

            return services;
        }

        
        public static IServiceCollection AddUnitOfWork<TContext1, TContext2, TContext3>(this IServiceCollection services)
            where TContext1 : DbContext
            where TContext2 : DbContext
            where TContext3 : DbContext
        {
            services.AddTransient<IFPUnitOfWork<TContext1>, FPUnitOfWork<TContext1>>();
            services.AddTransient<IFPUnitOfWork<TContext2>, FPUnitOfWork<TContext2>>();
            services.AddTransient<IFPUnitOfWork<TContext3>, FPUnitOfWork<TContext3>>();

            return services;
        }

        
        public static IServiceCollection AddUnitOfWork<TContext1, TContext2, TContext3, TContext4>(this IServiceCollection services)
            where TContext1 : DbContext
            where TContext2 : DbContext
            where TContext3 : DbContext
            where TContext4 : DbContext
        {
            services.AddTransient<IFPUnitOfWork<TContext1>, FPUnitOfWork<TContext1>>();
            services.AddTransient<IFPUnitOfWork<TContext2>, FPUnitOfWork<TContext2>>();
            services.AddTransient<IFPUnitOfWork<TContext3>, FPUnitOfWork<TContext3>>();
            services.AddTransient<IFPUnitOfWork<TContext4>, FPUnitOfWork<TContext4>>();

            return services;
        }
    }
}
