﻿using Microsoft.EntityFrameworkCore.Storage;
using ServiceStack.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Microsoft.EntityFrameworkCore
{

    public class FPUnitOfWork<TContext> : IFPRepositoryFactory, IFPUnitOfWork<TContext>, IFPUnitOfWork where TContext : DbContext
    {
        private readonly TContext _context;
        private bool disposed = false;
        private Dictionary<Type, object> repositories;

       
        public FPUnitOfWork(TContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }
        
        public TContext DbContext => _context;

        public String GetConnectionString()
        {
            return (_context.Database.GetDbConnection()).ConnectionString.ToString();
        }
        
        public IFPDbRepository<TEntity> GetRepository<TEntity>() where TEntity : class
        {
            if (repositories == null)
            {
                repositories = new Dictionary<Type, object>();
            }

            var type = typeof(TEntity);
            if (!repositories.ContainsKey(type))
            {
                repositories[type] = new FPDbRepository<TEntity>(_context);
            }

            return (IFPDbRepository<TEntity>)repositories[type];
        }
                   
        public int ExecuteSqlCommand(string sql, params object[] parameters) => _context.Database.ExecuteSqlCommand(sql, parameters);

        public IQueryable<TEntity> FromSql<TEntity>(string sql, params object[] parameters) where TEntity : class => _context.Set<TEntity>().FromSqlRaw(sql, parameters);
        
        public int SaveChanges(bool ensureAutoHistory = false)
        {
            if (ensureAutoHistory)
            {
                try
                {
                    _context.EnsureAutoHistory();
                }
                catch (OptimisticConcurrencyException ocEx)
                {
                    throw ocEx;
                }
                catch (DbUpdateException duEx)
                {
                    throw duEx;
                }
                catch (NotSupportedException nsEx)
                {
                    throw nsEx;
                }
                catch (ObjectDisposedException odEx)
                {
                    throw odEx;
                }
                catch (InvalidOperationException iopEx)
                {
                    throw iopEx;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            return _context.SaveChanges();
        }
        
        public async Task<int> SaveChangesAsync(bool ensureAutoHistory = false)
        {
            if (ensureAutoHistory)
            {
                _context.EnsureAutoHistory();
            }

            return await _context.SaveChangesAsync();
        }
        
        public async Task<int> SaveChangesAsync(bool ensureAutoHistory = false, params IFPUnitOfWork[] unitOfWorks)
        {
            // TransactionScope will be included in .NET Core v2.0
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    var count = 0;
                    foreach (var unitOfWork in unitOfWorks)
                    {
                        var uow = unitOfWork as FPUnitOfWork<DbContext>;
                        uow.DbContext.Database.UseTransaction(transaction.GetDbTransaction());
                        count += await uow.SaveChangesAsync(ensureAutoHistory);
                    }

                    count += await SaveChangesAsync(ensureAutoHistory);

                    transaction.Commit();

                    return count;
                }
                catch (Exception ex)
                {

                    transaction.Rollback();

                    throw ex;
                }
            }
        }
        
        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }
        
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    // clear repositories
                    if (repositories != null)
                    {
                        repositories.Clear();
                    }

                    // dispose the db context.
                    _context.Dispose();
                }
            }

            disposed = true;
        }
    }
}
