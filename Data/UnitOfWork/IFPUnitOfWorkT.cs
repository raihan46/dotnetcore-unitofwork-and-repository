﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Microsoft.EntityFrameworkCore
{

    public interface IFPUnitOfWork<TContext> : IFPUnitOfWork where TContext : DbContext
    {
        
        TContext DbContext { get; }
        
        Task<int> SaveChangesAsync(bool ensureAutoHistory = false, params IFPUnitOfWork[] unitOfWorks);
    }
}
