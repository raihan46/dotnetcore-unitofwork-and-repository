﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Microsoft.EntityFrameworkCore
{
    public interface IFPUnitOfWork : IDisposable
    {
        string GetConnectionString();
        IFPDbRepository<TEntity> GetRepository<TEntity>() where TEntity : class;        
        int SaveChanges(bool ensureAutoHistory = false);        
        Task<int> SaveChangesAsync(bool ensureAutoHistory = false);        
        int ExecuteSqlCommand(string sql, params object[] parameters);        
        IQueryable<TEntity> FromSql<TEntity>(string sql, params object[] parameters) where TEntity : class;
    }
}
