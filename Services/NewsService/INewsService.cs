﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using Models.News;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Services.NewsService
{
    public partial interface INewsService
    {
        #region NewsItem
        NewsItem GetNewsItemById(long Id);
        IPagedList<NewsItem> GetNewsItemList(Func<IQueryable<NewsItem>, IOrderedQueryable<NewsItem>> orderBy = null, Expression<Func<NewsItem, bool>> predicate = null, Func<IQueryable<NewsItem>, IIncludableQueryable<NewsItem, object>> include = null, int pageIndex = 0, int pageSize = 20);
        int GetNewsItemCount(Expression<Func<NewsItem, bool>> predicate = null);
        void AddNewsItem(NewsItem news);
        void UpdateNewsItem(NewsItem news);
        void DeleteNewsItem(NewsItem news);

        #endregion
    }
}
