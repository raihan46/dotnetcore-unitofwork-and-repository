﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using Models.News;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Services.NewsService
{
    public partial class NewsService : INewsService
    {

        private readonly IFPUnitOfWork _unitOfWork;

        // IFPRepositoryFactory is for readonly operations
        // IFPUnitOfWork is for read/write
        // IFPUnitOfWork<TContext> is for multiple databases contexts;
        public NewsService(IFPUnitOfWork unitOFWork)
        {
            _unitOfWork = unitOFWork;
        }

        #region Methods

        #region News                                  
        public virtual NewsItem GetNewsItemById(long Id)
        {
            if (Id == 0)
                return null;
            else
            {
                try
                {
                    return _unitOfWork.GetRepository<NewsItem>().Find(Id);
                }
                catch (InvalidOperationException iopExp)
                {
                    throw iopExp;
                }
            }
        }
        public virtual IPagedList<NewsItem> GetNewsItemList(Func<IQueryable<NewsItem>, IOrderedQueryable<NewsItem>> orderBy = null,
                                                                Expression<Func<NewsItem, bool>> predicate = null,
                                                                Func<IQueryable<NewsItem>, IIncludableQueryable<NewsItem, object>> include = null,
                                                                int pageIndex = 0, int pageSize = 20)
        {
            return _unitOfWork.GetRepository<NewsItem>().GetPagedList(predicate: predicate, include: include, orderBy: orderBy, pageIndex: pageIndex, pageSize: pageSize);
        }
        public int GetNewsItemCount(Expression<Func<NewsItem, bool>> predicate = null)
        {
            return _unitOfWork.GetRepository<NewsItem>().Count(predicate: predicate);
        }
        public virtual void AddNewsItem(NewsItem model)
        {
            _unitOfWork.GetRepository<NewsItem>().Insert(model);
            _unitOfWork.SaveChanges();
        }
        public virtual void UpdateNewsItem(NewsItem news)
        {
            if (news == null)
                throw new ArgumentNullException("No Data Object passed");
            else
            {
                _unitOfWork.GetRepository<NewsItem>().Update(news);
                _unitOfWork.SaveChanges();
            }
        }
        public virtual void DeleteNewsItem(NewsItem news)
        {
            if (news == null)
                throw new ArgumentNullException("No Data Object passed");
            else
            {
                _unitOfWork.GetRepository<NewsItem>().Delete(news);
                _unitOfWork.SaveChanges();
            }
        }

        #endregion

      

        #endregion
    }
}
