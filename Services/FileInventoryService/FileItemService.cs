﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using System;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Services.FileInventoryService
{
    public class FileItemService : IFileItemService
    {
        private readonly IHostingEnvironment _hostingEnvironment;

        public FileItemService( IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }


        #region Actual File Upload to Directory
       
        public string GetDirectoryForNewsFileUpload()
        {
            return @"wwwroot\Uploads\News\";
        }
        public string GetDirectoryForNewsFileDownload()
        {
            return @"Uploads\News\";
        }
     
        public bool? UploadFile(string filePath, IFormFile file, string epoch)
        {
            try
            {
                var webRootPath = _hostingEnvironment.WebRootPath; //returns -> F:\WorkStation\unitOfWork and Repository\Web\wwwroot
                var contentRootPath = _hostingEnvironment.ContentRootPath; //returns -> F:\WorkStation\unitOfWork and Repository\Web

                string path = Path.Combine(_hostingEnvironment.ContentRootPath, filePath);
                string fullPath = Path.Combine(_hostingEnvironment.ContentRootPath, filePath, epoch + "_" + file.FileName.ToLower());

                if (Directory.Exists(path))
                {
                    try
                    {
                        using (var stream = new FileStream(fullPath, FileMode.Create))
                        {
                            file.CopyTo(stream);
                            return true;
                        }
                    }
                    catch (ArgumentNullException anEx) { throw anEx; }
                    catch (PathTooLongException ptlEx) { throw ptlEx; }
                    catch (DirectoryNotFoundException dnfEx) { throw dnfEx; }
                    catch (UnauthorizedAccessException uaEx) { throw uaEx; }
                    catch (ArgumentException aEx) { throw aEx; }
                    catch (NotSupportedException nsupEx) { throw nsupEx; }
                    catch (System.Security.SecurityException secEx) { throw secEx; }
                    catch (FileNotFoundException fnfEx) { throw fnfEx; }
                    catch (IOException ioEx) { throw ioEx; }
                }
                else
                {

                    try
                    {
                        Directory.CreateDirectory(path);
                        try
                        {
                            using (var stream = new FileStream(fullPath, FileMode.Create))
                            {
                                file.CopyTo(stream);
                                return true;
                            }
                        }
                        catch (System.Security.SecurityException secEx) { throw secEx; }
                        catch (FileNotFoundException fnfEx) { throw fnfEx; }
                    }
                    catch (ArgumentNullException anEx) { throw anEx; }
                    catch (DirectoryNotFoundException dnfEx) { throw dnfEx; }
                    catch (PathTooLongException ptlEx) { throw ptlEx; }
                    catch (IOException ioEx) { throw ioEx; }
                    catch (UnauthorizedAccessException uaEx) { throw uaEx; }
                    catch (ArgumentException aEx) { throw aEx; }
                    catch (NotSupportedException nsEx) { throw nsEx; }

                }
            }
            catch (UnauthorizedAccessException uaEx) { throw uaEx; }
            catch (IOException ioEx) { throw ioEx; }
            catch (Exception ex) { throw ex; }
        }
        #endregion

        #region MetaData
        public string[] GetUploadTypes()
        {
            return new[] { "image", "document" };
        }

        public string[] GetImageTypes()
        {
            return new[] { ".jpg", ".jpeg", ".bmp", ".png" };
        }

        public string[] GetDocumentTypes()
        {
            return new[] { ".doc", ".docx", ".xls", ".xlsx", ".pdf", ".zip" };
        }
        #endregion
    }
}
