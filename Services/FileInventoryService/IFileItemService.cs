﻿using Microsoft.AspNetCore.Http;

namespace Services.FileInventoryService
{
    public interface IFileItemService
    {
        #region Actual File Upload to Directory       
        string GetDirectoryForNewsFileUpload();
        string GetDirectoryForNewsFileDownload();

        bool? UploadFile(string filePath, IFormFile file, string epoch);
        #endregion

        #region MetaData
        string[] GetUploadTypes();
        string[] GetImageTypes();
        string[] GetDocumentTypes();
        #endregion
    }
}
