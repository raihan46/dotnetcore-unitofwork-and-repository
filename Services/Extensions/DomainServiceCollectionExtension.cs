﻿using Microsoft.Extensions.DependencyInjection;

namespace Microsoft.EntityFrameworkCore
{
    public static class DomainServiceCollectionExtension
    {
        public static IServiceCollection AddAllInformationBasedDomainServices(this IServiceCollection services)
        {
            //register all services required in the solution

            //services.AddTransient<>();
            //services.AddTransient<IFileItemService, FileItemService>();


            services.AddTransient<Services.FileInventoryService.IFileItemService, Services.FileInventoryService.FileItemService>();
            
            services.AddTransient<Services.NewsService.INewsService, Services.NewsService.NewsService>();
            return services;
        }
    }
}
