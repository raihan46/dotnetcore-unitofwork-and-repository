﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;

namespace Services.SP
{
    public partial class SPService : ISPService
    {
        private readonly string _connectionString;

        public SPService(IFPUnitOfWork unitOfWork)
        {
            _connectionString = unitOfWork.GetConnectionString();
        }

        /// <summary>
        /// Get only DataTable from Stored Procedure
        /// </summary>
        /// <param name="SPName">Stored Procedure Name</param>
        /// <param name="paramList">Parameter List</param>
        /// <returns>Returns DataTable</returns>
        public virtual DataTable GetDataListFromSP(string SPName, List<SqlParameter> paramList)
        {
            DataTable dt = new DataTable();
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand(SPName, connection))
                {
                    foreach (var item in paramList)
                    {
                        command.Parameters.Add(item);
                    }

                    connection.Open();
                    using (SqlTransaction objTrans = connection.BeginTransaction(IsolationLevel.ReadCommitted))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Transaction = objTrans;
                        try
                        {
                            using (SqlDataReader reader = command.ExecuteReader())
                            {
                                dt.Load(reader);
                            }
                            objTrans.Commit();
                        }
                        catch (Exception ex)
                        {
                            objTrans.Rollback();
                        }
                    }
                }
            }
            return dt;
        }
        /// <summary>
        /// Get only Generic List from Stored Procedure
        /// </summary>
        /// <typeparam name="T">Generic Class Name</typeparam>
        /// <param name="SPName">Stored Procedure Name</param>
        /// <param name="paramList">Parameter List</param>
        /// <returns>Returns Generic List</returns>
        public List<T> GetListFromSP<T>(string SPName, List<SqlParameter> paramList)
        {
            List<T> list = new List<T>();
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand(SPName, connection))
                {
                    foreach (var item in paramList)
                    {
                        command.Parameters.Add(item);
                    }

                    connection.Open();
                    using (SqlTransaction objTrans = connection.BeginTransaction(IsolationLevel.ReadCommitted))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Transaction = objTrans;
                        try
                        {
                            using (SqlDataReader dr = command.ExecuteReader())
                            {
                                while (dr.Read())
                                {
                                    var destination = Activator.CreateInstance(typeof(T));
                                    var propertyList = destination.GetType().GetProperties().ToList();
                                    foreach (PropertyInfo prop in propertyList)
                                    {
                                        if (dr.GetSchemaTable().Select("ColumnName = '" + prop.Name + "'").Count() == 1)
                                            prop.SetValue(destination, (dr[prop.Name] == DBNull.Value) ? null : dr[prop.Name], null);
                                    }
                                    list.Add((T)destination);
                                }
                            }
                            objTrans.Commit();
                        }
                        catch (Exception ex)
                        {
                            objTrans.Rollback();
                        }
                    }
                }
            }
            return list;
        }



        public virtual DataTable GetSingleDataFromQuery(string sqlText)
        {
            DataTable dt = new DataTable();
            if (string.IsNullOrWhiteSpace(sqlText))
                return dt;

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand(sqlText, connection))
                {
                    connection.Open();
                    using (SqlTransaction objTrans = connection.BeginTransaction(IsolationLevel.ReadCommitted))
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandTimeout = 0;
                        command.Transaction = objTrans;
                        try
                        {
                            using (SqlDataReader reader = command.ExecuteReader())
                            {
                                dt.Load(reader);
                            }
                            objTrans.Commit();
                        }
                        catch (Exception ex)
                        {
                            objTrans.Rollback();
                        }
                    }
                }
            }
            return dt;
        }

        public virtual int ExecuteNoQuery(string sqlText, List<SqlParameter> parameterList)
        {
            int result = -1;
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand(sqlText, connection))
                {
                    foreach (var item in parameterList)
                    {
                        command.Parameters.Add(item);
                    }
                    connection.Open();
                    using (SqlTransaction objTrans = connection.BeginTransaction(IsolationLevel.ReadCommitted))
                    {
                        command.CommandType = CommandType.Text;
                        command.Transaction = objTrans;
                        try
                        {
                            result = command.ExecuteNonQuery();
                            objTrans.Commit();
                        }
                        catch (Exception ex)
                        {
                            objTrans.Rollback();
                        }
                    }
                }
            }
            return result;
        }
        
        public virtual List<T> GetDataFromQuery<T>(string sqlText)
        {
            List<T> list = new List<T>();
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand(sqlText, connection))
                {
                    connection.Open();
                    using (SqlTransaction objTrans = connection.BeginTransaction(IsolationLevel.ReadCommitted))
                    {
                        command.CommandType = CommandType.Text;
                        command.Transaction = objTrans;
                        try
                        {
                            using (SqlDataReader dr = command.ExecuteReader())
                            {
                                while (dr.Read())
                                {
                                    var destination = Activator.CreateInstance(typeof(T));
                                    var propertyList = destination.GetType().GetProperties().ToList();
                                    foreach (PropertyInfo prop in propertyList)
                                    {
                                        if (dr.GetSchemaTable().Select("ColumnName = '" + prop.Name + "'").Count() == 1)
                                            prop.SetValue(destination, (dr[prop.Name] == DBNull.Value) ? null : dr[prop.Name], null);
                                    }
                                    list.Add((T)destination);
                                }
                            }
                            objTrans.Commit();
                        }
                        catch (Exception ex)
                        {
                            objTrans.Rollback();
                        }
                    }
                }
            }
            return list;
        }
        
        public virtual T GetDataFromQuerySingleObject<T>(string sqlText)
        {
            T destination = (T)Activator.CreateInstance(typeof(T));
            //SqlTransaction objTrans = null;
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand(sqlText, connection))
                {
                    // Start a local transaction.
                    connection.Open();
                    using (SqlTransaction objTrans = connection.BeginTransaction(IsolationLevel.ReadCommitted))
                    {
                        command.Transaction = objTrans;
                        command.CommandType = CommandType.Text;

                        try
                        {
                            using (SqlDataReader dr = command.ExecuteReader())
                            {
                                if (dr.Read())
                                {
                                    var propertyList = destination.GetType().GetProperties().ToList();
                                    foreach (PropertyInfo prop in propertyList)
                                    {
                                        if (dr.GetSchemaTable().Select("ColumnName = '" + prop.Name + "'").Count() == 1)
                                            prop.SetValue(destination, (dr[prop.Name] == DBNull.Value) ? null : dr[prop.Name], null);
                                    }
                                }
                            }
                            objTrans.Commit();
                        }
                        catch (Exception ex)
                        {
                            objTrans.Rollback();
                        }
                    }
                }
            }
            return destination;
        }

        public virtual object GetScalarData(string cmd)
        {
            object result = null;
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand(cmd, connection))
                {
                    connection.Open();
                    using (SqlTransaction objTrans = connection.BeginTransaction(IsolationLevel.ReadCommitted))
                    {
                        command.CommandType = System.Data.CommandType.Text;
                        command.Transaction = objTrans;
                        try
                        {
                            result = command.ExecuteScalar();
                            objTrans.Commit();
                        }
                        catch (Exception ex)
                        {
                            objTrans.Rollback();
                        }
                    }
                }
            }
            return result;
        }

    }
}
