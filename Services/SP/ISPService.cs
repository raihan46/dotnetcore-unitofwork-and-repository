﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Services.SP
{
    public partial interface ISPService
    {
        /// <summary>
        /// Get only DataTable from Stored Procedure
        /// </summary>
        /// <param name="SPName">Stored Procedure Name</param>
        /// <param name="paramList">Parameter List</param>
        /// <returns>Returns DataTable</returns>
        DataTable GetDataListFromSP(string SPName, List<SqlParameter> paramList);
        /// <summary>
        /// Get only Generic List from Stored Procedure
        /// </summary>
        /// <typeparam name="T">Generic Class Name</typeparam>
        /// <param name="SPName">Stored Procedure Name</param>
        /// <param name="paramList">Parameter List</param>
        /// <returns>Returns Generic List</returns>
        List<T> GetListFromSP<T>(string SPName, List<SqlParameter> paramList);


        DataTable GetSingleDataFromQuery(string sqlText);
        List<T> GetDataFromQuery<T>(string sqlText);
        T GetDataFromQuerySingleObject<T> (string sqlText);
        int ExecuteNoQuery(string sqlText, List<SqlParameter> parameterList);
        object GetScalarData(string cmd);
    }
}
