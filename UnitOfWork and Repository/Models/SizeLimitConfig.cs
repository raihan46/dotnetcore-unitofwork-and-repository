﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UnitOfWork_and_Repository.Models
{
    public class SizeLimitConfig
    {
        public long PDFSizeLimit { get; set; }
        public long MarketPulseSizeLimit { get; set; }
        public long TextFileSizeLimit { get; set; }
        public long ImageSizeLimit { get; set; }
        public long CoverPhotoSizeLimit { get; set; }
        public long ThumbnailSizeLimit { get; set; }
    }
}
