﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.News
{
    public partial class NewsType : BaseModel
    {
        public string NewsTypeName { get; set; }

        public virtual ICollection<NewsItem> NewsItems { get; set; }
    }
}
