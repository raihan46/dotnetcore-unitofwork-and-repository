﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Models.News
{
    public partial class NewsComment : BaseModel
    {                  
        public string CommentTitle { get; set; }
        public string CommentText { get; set; }
        public int CustomerId { get; set; }
        public bool IsApproved { get; set; }

        public long NewsId { get; set; }

        public virtual NewsItem NewsItem { get; set; }
                                                  
    }
}
