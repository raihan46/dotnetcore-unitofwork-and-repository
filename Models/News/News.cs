﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.News
{
    public partial class NewsItem  : BaseModel
    {
        public NewsItem()
        {
            //1
            //NewsComments = new HashSet<NewsComment>();
        }
        public long? NewsTypeId { get; set; }
        public string Title { get; set; }
        public string ImageUrl { get; set; }
        public string Description { get; set; }
        public DateTime? NewsDateTime { get; set; }
        public string SourceName { get; set; }
        public string SourceURL { get; set; }
        // alternative to comment 1,2,3, 
        //FYI: comment out 1,2 & 3 to use this following format
        public virtual ICollection<NewsComment> NewsComments { get; set; }
        public virtual NewsType NewsType { get; set; }
        //2
        //private ICollection<NewsComment> _newsComments;
        //3
        //public virtual ICollection<NewsComment> NewsComments
        //{
        //    get { return _newsComments ?? (_newsComments = new List<NewsComment>()); }
        //    protected set { _newsComments = value; }
        //}

    }
}
