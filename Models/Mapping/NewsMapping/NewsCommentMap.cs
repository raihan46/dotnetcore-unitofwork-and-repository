﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Models.News;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Mapping.NewsMapping
{
    public class NewsCommentMap : IEntityTypeConfiguration<NewsComment>
    {
        public void Configure(EntityTypeBuilder<NewsComment> builder)
        {
            builder.HasOne(a => a.NewsItem)
                .WithMany(a => a.NewsComments)
                .HasForeignKey(a => a.NewsId);
        }
    }
}
