﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Models.News;

namespace Models.Mapping.NewsMaping
{
    public class NewsItemMap : IEntityTypeConfiguration<NewsItem>
    {                   
        public void Configure(EntityTypeBuilder<NewsItem> builder)
        {   
            builder.Property(b => b.Title).IsRequired().HasMaxLength(200);

            builder.HasMany(a => a.NewsComments)
                .WithOne(a => a.NewsItem)
                .HasForeignKey(a=>a.NewsId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasOne(a => a.NewsType)
                .WithMany(a => a.NewsItems)
                .HasForeignKey(a => a.NewsTypeId);
        }
    }
}
