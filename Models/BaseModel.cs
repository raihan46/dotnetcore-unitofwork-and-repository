﻿using ServiceStack.DataAnnotations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Models
{
    public abstract partial class BaseModel
    {
        [ScaffoldColumn(false)]
        public long Id { get; set; }
        [ScaffoldColumn(false)]
        public DateTime? CreatedDate { get; set; }
        [ScaffoldColumn(false)]
        public string CreatedBy { get; set; }
        [ScaffoldColumn(false)]
        public DateTime? LastModifiedDate { get; set; }
        [ScaffoldColumn(false)]
        public string LastModifiedBy { get; set; }

        [ScaffoldColumn(false)]
        [Timestamp]
        public byte[] TimeStamp { get; set; }
    }
}
